from django.apps import AppConfig


class EbaiiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ebaii'
